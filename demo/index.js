var express = require('express');
var PORT = process.env.PORT || 3000,
    app = express();

app.use('/', express.static('static'));

app.listen(PORT, function () {
  console.log('Example app listening on port 3000!');
});

angular.module('umbrella').constant('CONTRACTS', {
  aContractId: {
    id: 'aContractId',
    name: 'Pack Numérique',
    url: 'https://www.axa.fr/assurance-habitation/packs/appareils-numeriques.html',
    protections: [{
      text: 'vol'
    }, {
      text: 'casse accidentelle'
    }],
    more: 'Un téléphone vous est prêté gratuitement et vous est livré sur le lieu de votre choix'
  },
  anotherContractId: {
    id: 'anotherContractId',
    name: 'Pack Numérique',
    url: 'https://www.axa.fr/assurance-habitation/packs/appareils-numeriques.html',
    protections: [{
      text: 'vol'
    }, {
      text: 'casse accidentelle'
    }]
  },
});

angular.module('umbrella').controller('AppCtrl', function(
  $scope,
  $state,
  userService
) {
  // Exposed globally
  $scope.currentUser = null;

  $scope.login = function() {
    if (userService.isLoggedIn()) {
      return;
    }

    $state.transitionTo('app.login');
  }

  $scope.logout = function() {
    $state.transitionTo('app.home');
    userService.logout();
  }

  $scope.$watch(function() {
    return userService.getUser();
  }, function(user) {
    $scope.currentUser = user;
  }, true);

});

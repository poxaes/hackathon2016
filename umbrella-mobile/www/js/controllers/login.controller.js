angular.module('umbrella').controller('LoginCtrl', function(
  $scope,
  userService,
  $state,
  $ionicHistory
) {

  $scope.login = {};

  $scope.submit = function() {
    return userService.login($scope.login.id, $scope.login.password)
      .then(function () {
        $ionicHistory.goBack();
      });
  }
});

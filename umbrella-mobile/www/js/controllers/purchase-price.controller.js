angular.module('umbrella').controller('PurchasePriceCtrl', function (
  $scope,
  $stateParams
) {
  var PRICES_ATTR = [{
    id: 'telephone',
    label: 'smartphone',
    prices: [{
      min: 0,
      max: 100
    },{
      min: 100,
      max: 400
    },{
      min: 400,
      max: 1000
    }]
  }, {
    id: 'tablette',
    label: 'tablette',
    prices: [{
      min: 0,
      max: 30
    }, {
      min: 30,
      max: 200
    }, {
      min: 200,
      max: 700
    }, {
      min: 700,
      max: 1000
    }]
  }, {
    id: 'appareilPhoto',
    label: 'appareil photo',
    prices: [{
      min: 0,
      max: 50
    },{
      min: 50,
      max: 500
    },{
      min: 500,
      max: 2000
    }]
  }, {
    id: 'ordinateur',
    label: 'ordinateur',
    prices: [{
      min: 0,
      max: 100
    },{
      min: 100,
      max: 1500
    },{
      min: 1500,
      max: 4000
    }]
  }, {
    id: 'television',
    label: 'télévision',
    prices: [{
      min: 0,
      max: 100
    },{
      min: 100,
      max: 400
    }, {
      min: 400,
      max: 1000
    }, {
      min: 1000,
      max: 3000
    }]
  }, {
    id: 'console',
    label: 'console de jeux',
    prices: [{
      min: 0,
      max: 100
    },{
      min: 100,
      max: 400
    },{
      min: 400,
      max: 1000
    }]
  }],

  PRICES = PRICES_ATTR.reduce(function (acc, p) {
    acc[p.id] = p;
    return acc;
  }, {});

  $scope.cat = PRICES[$stateParams.cat];

});

angular.module('umbrella').controller('PurchaseSearchCtrl', function (
  $scope,
  productService
) {
  function search(searchTerm) {
    return productService.search(searchTerm)
      .then(function (products) {
        $scope.searchedProducts = products.filter(function(product) {
          return product.name && product.category;
        });
      });
  }

  $scope.onSearchChange = search;
});

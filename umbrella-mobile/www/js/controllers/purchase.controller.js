angular.module('umbrella').controller('PurchaseCtrl', function(
  $scope,
  $ionicPlatform,
  $q,
  $state,
  $window,
  $cordovaBarcodeScanner
) {
  function goToProductPage(productId) {
    return $state.transitionTo('app.result', {productId: productId});
  }

  function scanBarcode() {
    if (!$window.cordova) {
      return $q.resolve('ok').then(goToProductPage);
    }

    return $ionicPlatform.ready()
      .then(function() {
        return $cordovaBarcodeScanner.scan();
      })
      .then(function(data) {
        if (data.cancelled) {
          return;
        }

        return goToProductPage(data.text);
      });
  }

  $scope.scanBarcode = scanBarcode;
});

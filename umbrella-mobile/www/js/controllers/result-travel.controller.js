angular.module('umbrella').controller('ResultTravelCtrl', function(
  $scope,
  $ionicLoading,
  $stateParams,
  $window,
  userService,
  $timeout
) {
  var TYPES = {
    AWAY: 'voyage à l\'étranger',
    MOUNTAIN: 'Ski et Montagne'
  }, CB = {
      REGULAR: 'Classique',
      GOLD: 'Premium'
  }, PRICE = {
    MIN: 'moins de 5000€',
    MAX: 'plus de 5000€'
  }, PRODUCTS = {
    AWAY: {
      id: 'ok',
      name: '',
      description: '',
      coveredBy: {
        id: 'aContractId',
        name: 'Voyageo',
        protections: [{
          text: 'Annulation / interruption de séjour',
          subtext: 'à hauteur de 5 000€ par assuré',
        }, {
          text: 'Retard de vol',
          subtext: 'à hauteur de 400€ par assuré'
        }, {
          text: 'Perte ou vol de vos baguages',
          subtext: 'à hauteur de 800€ par assuré'
        }],
        more: 'Bénéficiez d\'une assistance 24/24 à 7/7 où que vous soyez dans le monde.'
      }
    },
    AWAYCOVERED: {
      id: 'ok',
      name: '',
      description: '',
      coveredBy: {
        id: 'aContractId',
        name: 'Voyageo',
        protections: [{
          text: 'Annulation / interruption de séjour',
          subtext: 'à hauteur de 8 000€ par assuré',
        }, {
          text: 'Départ ou retour impossible',
          subtext: 'à hauteur de 2 000€ par assuré'
        }, {
          text: 'Perte ou vol de vos baguages',
          subtext: 'à hauteur de 2 000€ par assuré'
        }],
        more: 'Bénéficiez d\'une assistance 24/24 à 7/7 où que vous soyez dans le monde.'
      }
    }, MOUNTAIN: {
      id: 'ok',
      name: '',
      description: '',
      coveredBy: {
        id: 'bContractId',
        name: 'Voyageo neige',
        protections: [{
          text: 'Accident nécessitant un secours sur pistes ou hors-pistes'
        }, {
          text: 'D\'accident empêchant la pratique du ski',
          subtext: 'à hauteur de 500€ par personne et par évènement'
        }, {
          text: 'Bris et vol de votre matériel',
          subtext: 'à hauteur de 400€ par personne et par évènement'
        }],
        more: 'Bénéficiez d\'une assistance 24/24 à 7/7 où que vous soyez en Europe.'
      }
    }
  };

  var type = TYPES[$stateParams.type],
      cb = CB[$stateParams.cb],
      price = PRICE[$stateParams.price],
      user = userService.getUser(),
      product = PRODUCTS[$stateParams.type];

    function refreshData() {
      $scope.product = null;
      $scope.isCovered = null;
      $scope.dataLoaded = false;
      $scope.isLogged = !!userService.getUser();

      $ionicLoading.show()
      $timeout(Math.floor((Math.random()*600)+300))
        .then(function() {
          $scope.dataLoaded = true;
          $scope.type = type;
          $scope.product = product;
          $scope.cb = cb;
          $scope.price = price;
          $scope.hasGold = $stateParams.cb === 'GOLD';
          $scope.isCovered = user && $stateParams.cb != 'REGULAR' && $stateParams.price === 'MIN';
        })
        .finally(function() {
          $ionicLoading.hide();
        });
    }

    function subscribe() {
      var url = 'https://www.assurance-voyage.axa-assistance.fr/insurance';
      $window.open(url, '_system', 'location=yes');
    }

    $scope.subscribe = subscribe;

    $scope.$on('$ionicView.enter', function() {
      refreshData();
    });

});

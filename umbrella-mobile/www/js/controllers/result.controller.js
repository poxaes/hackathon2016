angular.module('umbrella').controller('ResultCtrl', function(
  $scope,
  $ionicLoading,
  $stateParams,
  $window,
  userService,
  productService,
  CONTRACTS
) {

  function isProductCovered(user, product) {
    return user && user.contracts && user.contracts.indexOf(product.coveredBy.id) !== -1;
  }

  function refreshData() {
    $scope.product = null;
    $scope.isCovered = null;
    $scope.dataLoaded = false;
    $scope.isLogged = !!userService.getUser();

    $ionicLoading.show()
    productService.getProduct($stateParams.productId)
      .then(function(product) {
        $scope.dataLoaded = true;
        $scope.product = product;
        $scope.isCovered = isProductCovered(userService.getUser(), product);
      })
      .finally(function() {
        $ionicLoading.hide();
      });
  }

  function subscribe() {
    var url = $scope.product ? $scope.product.coveredBy.url : 'https://www.axa.fr';
    $window.open(url, '_system', 'location=yes');
  }

  $scope.subscribe = subscribe;

  $scope.$on('$ionicView.enter', function() {
    refreshData();
  });
});

angular.module('umbrella').filter('highlight', function($sce) {
  return function(text, highlightedText) {
    if (highlightedText) {
      text = text.replace(
        new RegExp('(' + highlightedText + ')', 'gi'),
        '<mark>$1</mark>'
      );
    }

    return $sce.trustAsHtml(text);
  }
})

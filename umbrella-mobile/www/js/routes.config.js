angular.module('umbrella').config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/app.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html'
      }
    }
  })

  .state('app.purchase', {
    url: '/purchase',
    views: {
      'menuContent': {
        templateUrl: 'templates/purchase.html',
        controller: 'PurchaseCtrl'
      }
    }
  })
  .state('app.purchase.price', {
    url: '/:cat',
    views: {
      'menuContent@app': {
        templateUrl: 'templates/price.purchase.html',
        controller: 'PurchasePriceCtrl'
      }
    }
  })
  .state('app.purchaseSearch', {
    url: '/purchase/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/purchase-search.html',
        controller: 'PurchaseSearchCtrl'
      }
    }
  })

  .state('app.travel', {
    url: '/travel',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/home.travel.html'
      }
    }
  })

  .state('app.travel.cb', {
    url: '/:type/cb',
    cache: false,
    views: {
      'menuContent@app': {
        templateUrl: 'templates/cb.travel.html',
        controller: function($scope, $stateParams, $state) {
            $scope.setCb = function(cb) {
              if($stateParams.type === 'MOUNTAIN') {
                $state.go('app.travel.cb.price.result', {price: 'MAX'});
              } else {
                $state.go('app.travel.cb.price', {cb: cb});
              }
            }
        }
      }
    }
  })
  .state('app.travel.cb.price', {
    url: '/:cb/price',
    cache: false,
    views: {
      'menuContent@app': {
        templateUrl: 'templates/price.travel.html'
      }
    }
  })
  .state('app.travel.cb.price.result', {
    url: '/:price/result',
    cache: false,
    views: {
      'menuContent@app': {
        templateUrl: 'templates/result.travel.html',
        controller: 'ResultTravelCtrl'
      }
    }
  })
  .state('app.transport', {
    url: '/transport',
    views: {
      'menuContent': {
        templateUrl: 'templates/transport.html'
      }
    }
  })

  .state('app.result', {
    url: '/result/:productId',
    views: {
      'menuContent': {
        templateUrl: 'templates/result.html',
        controller: 'ResultCtrl'
      }
    }
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  .state('app.category-empty', {
    url: '/caterory',
    views: {
      'menuContent': {
        templateUrl: 'templates/empty-data.html'
      }
    }
  })

  $ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.backButton.text('Retour')

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});

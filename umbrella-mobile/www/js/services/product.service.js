angular.module('umbrella').factory('productService', function(
  $timeout,
  $q,
  CONTRACTS
) {
  var api = {},
    PRODUCT_COVERED = {
      id: 'ok',
      type: 'smartphone',
      name: 'Samsung Galaxy S6',
      category: {
        id: 'electronics',
        name: 'Électronique'
      },
      description: '',
      coveredBy: CONTRACTS.aContractId
    },
    PRODUCT_NOT_COVERED = {
      id: 'ko',
      type: 'tablette',
      name: 'Samsung Galaxy Tab',
      category: {
        id: 'electronics',
        name: 'Électronique'
      },
      description: '',
      coveredBy: CONTRACTS.aContractId
    },
    PRODUCTS_ARR = [
      PRODUCT_COVERED,
      PRODUCT_NOT_COVERED, {
        id: 'ok1',
        type: 'smartphone',
        name: 'Samsung Galaxy Note 4',
        category: {
          id: 'electronics',
          name: 'Électronique'
        },
        description: '',
        coveredBy: CONTRACTS.aContractId
      }, {
        id: 'ok2',
        type: 'tablette',
        name: 'Samsung Galaxy Tab A 2016',
        category: {
          id: 'electronics',
          name: 'Électronique'
        },
        description: '',
        coveredBy: CONTRACTS.aContractId
      },{
        id: 'ok3',
        type: 'smartwatch',
        name: 'Samsung Gear S2',
        category: {
          id: 'electronics',
          name: 'Électronique'
        },
        description: '',
        coveredBy: CONTRACTS.anotherContractId
      }, {
        id: 'telephone',
        type: 'smartphone',
        coveredBy: CONTRACTS.aContractId
      }, {
        id: 'tablette',
        type: 'tablette',
        coveredBy: CONTRACTS.anotherContractId
      }, {
        id: 'appareilPhoto',
        type: 'appareil photo',
        coveredBy: CONTRACTS.anotherContractId
      }, {
        id: 'ordinateur',
        type: 'ordinateur',
        coveredBy: CONTRACTS.anotherContractId
      }, {
        id: 'television',
        type: 'télévision',
        coveredBy: CONTRACTS.anotherContractId
      }, {
        id: 'console',
        type: 'console de jeux',
        coveredBy: CONTRACTS.anotherContractId
      }
    ],
    PRODUCTS = PRODUCTS_ARR.reduce(function (acc, p) {
      acc[p.id] = p;
      return acc;
    }, {});

  function values(obj) {
    return Object.keys(obj).map(function (key) {
      return obj[key];
    });
  }

  api.getProduct = function(id) {
    return $timeout(Math.floor((Math.random()*600)+300))
      .then(function() {
        var product = PRODUCTS[id];

        return product || PRODUCT_COVERED;
      });
  };

  api.search = function (searchTerm) {
    return $timeout(Math.floor((Math.random()*200)+100))
      .then(function () {
        return searchTerm ? values(PRODUCTS) : [];
      });
  };

  return api;
});

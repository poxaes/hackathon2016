angular.module('umbrella').factory('userService', function(
  $timeout,
  $localStorage
) {
  var api = {},
    user = $localStorage.user || null;

  api.login = function(id, password) {
    return $timeout(Math.floor((Math.random()*600)+300))
      .then(function () {
        user = {
          name: 'Booba Lours',
          picture: 'img/user-logged.png',
          contracts: ['aContractId', 'anotherContractId'],
          anonymous: false
        };
        $localStorage.user = user;

        return user;
      });
  };

  api.logout = function () {
    delete $localStorage.user;
    user = null;
  };

  api.isLoggedIn = function () {
    return !!user;
  };

  api.getUser = function() {
    return user;
  };

  return api;
});
